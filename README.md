# Webpack = Angular + Bootstrap + Your Custom Style [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome)
* This Branch contains development code.

![Official Dependency Tree](http://i.imgur.com/YU4xBPQ.png)

This Package Contains Webpack Development Server which include together bundle of AngularJs v1.6 and Bootstrap v3. This is Developed for the purpose of bundling the assets as mention in webpackjs.org as this is the popular framework for bundling the assests in 1 file. 

### Essetials Tools
* nodeJS : [Download from nodejs.org](https://nodejs.org/en/download/)
* git : [Download from git-scm.com](https://git-scm.com/) 

# How to Setup :-

* Clone this repository using git clone <-this repo link-> your-folder-name
* Now cd to your folder : $>cd your-folder-name

* Run the command

```
 npm install

```
 * The above command will install necessary node_modules , now run the following command
  
```
npm run dev

``` 
 * Now the enjoy the app at http://localhost:8090/

 * You can run these commands with

```
npm run build

```
* Above `npm run build` will run `webpack.config.prod.js` and build all files in `dist` directory.

```
npm run dev
```

* Above `npm run dev` will start Development environment which will fire `webpack.config.dev.js`

