// webpack.config.prod.js
var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  devtool: 'source-map',
    entry: {
    app: './src/main.js',
    vendor: ['angular','bootstrap']
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: "[name].js"
  },
    plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false,
      },
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.CommonsChunkPlugin("vendor"),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      inject: 'head',  
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({   
        jQuery: 'jquery',
        $: 'jquery',
        jquery: 'jquery',
    })
  ],
  
   module: {
    rules: [
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
        {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        loader: 'url-loader',
        options: {
          limit: 10000
        }
      }
    ]
  },
}